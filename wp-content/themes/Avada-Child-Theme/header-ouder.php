<!DOCTYPE html>
<?php global $woocommerce;

	$_SESSION["kenteken"] = "";
	$_SESSION["merk"] = "";
	$_SESSION["handelsbenaming"] = "";
	$_SESSION["bouwjaar"] = "";
	$_SESSION["lediggewicht"] = "";
	$_SESSION["nieuwwaarde"] = "";
	$_SESSION["motorinhoud"] = "";

	require_once("socrata.php");

	$view_uid = "m9d7-ebf2";
	$root_url = "https://opendata.rdw.nl";
	$app_token = "aHEWbjMHCJfM2WpVla41EQtUy";
	$response = NULL;

	$kenteken = array_get("kenteken", $_POST);

	if($kenteken != NULL) {
		// Create a new unauthenticated client
		$socrata = new Socrata($root_url, $app_token);
		$params = array("kenteken" => $kenteken);
		$response = $socrata->get("/resource/$view_uid.json", $params);
		foreach($response as $row) {
			$_SESSION["kenteken"] = $row["kenteken"];
			$_SESSION["merk"] = $row["merk"];
			$_SESSION["handelsbenaming"] = $row["handelsbenaming"];
			$_SESSION["bouwjaar"] = $row["datum_eerste_afgifte_nederland"];
			$_SESSION["lediggewicht"] = $row["massa_ledig_voertuig"];
			$_SESSION["nieuwwaarde"] = $row["catalogusprijs"];
			$_SESSION["motorinhoud"] = $row["cilinderinhoud"];
			//echo '<pre>',print_r($row, 1),'</pre>';
		}
	}
	// echo '<pre>',print_r($_POST, 1),'</pre>';
	// echo '<pre>',print_r($_SESSION, 1),'</pre>';
 ?>
<html class="<?php echo ( ! Avada()->settings->get( 'smooth_scrolling' ) ) ? 'no-overflow-y' : ''; ?>" xmlns="http<?php echo (is_ssl())? 's' : ''; ?>://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<?php if ( isset( $_SERVER['HTTP_USER_AGENT'] ) && ( false !== strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE' ) ) ) : ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<?php endif; ?>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<?php
	if ( ! function_exists( '_wp_render_title_tag' ) ) {
		function avada_render_title() {
		?>
			<title><?php wp_title( '' ); ?></title>
		<?php
		}
		add_action( 'wp_head', 'avada_render_title' );
	}
	?>

	<!--[if lte IE 8]>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/html5shiv.js"></script>
	<![endif]-->

	<?php $isiPad = (bool) strpos( $_SERVER['HTTP_USER_AGENT'],'iPad' ); ?>

	<?php
	$viewport = '';
	if ( Avada()->settings->get( 'responsive' ) && $isiPad ) {
		$viewport = '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />';
	} else if( Avada()->settings->get( 'responsive' ) ) {
		if ( Avada()->settings->get( 'mobile_zoom' ) ) {
			$viewport .= '<meta name="viewport" content="width=device-width, initial-scale=1" />';
		} else {
			$viewport .= '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />';
		}
	}

	$viewport = apply_filters( 'avada_viewport_meta', $viewport );
	echo $viewport;
	?>

	<?php if ( Avada()->settings->get( 'favicon' ) ) : ?>
		<link rel="shortcut icon" href="<?php echo Avada()->settings->get( 'favicon' ); ?>" type="image/x-icon" />
	<?php endif; ?>

	<?php if ( Avada()->settings->get( 'iphone_icon' ) ) : ?>
		<!-- For iPhone -->
		<link rel="apple-touch-icon-precomposed" href="<?php echo Avada()->settings->get( 'iphone_icon' ); ?>">
	<?php endif; ?>

	<?php if ( Avada()->settings->get( 'iphone_icon_retina' ) ) : ?>
		<!-- For iPhone 4 Retina display -->
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo Avada()->settings->get( 'iphone_icon_retina' ); ?>">
	<?php endif; ?>

	<?php if ( Avada()->settings->get( 'ipad_icon' ) ) : ?>
		<!-- For iPad -->
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo Avada()->settings->get( 'ipad_icon' ); ?>">
	<?php endif; ?>

	<?php if ( Avada()->settings->get( 'ipad_icon_retina' ) ) : ?>
		<!-- For iPad Retina display -->
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo Avada()->settings->get( 'ipad_icon_retina' ); ?>">
	<?php endif; ?>

	<?php wp_head(); ?>

	<?php

	$object_id = get_queried_object_id();
	$c_pageID  = Avada::c_pageID();
	?>

	<!--[if lte IE 8]>
	<script type="text/javascript">
	jQuery(document).ready(function() {
	var imgs, i, w;
	var imgs = document.getElementsByTagName( 'img' );
	for( i = 0; i < imgs.length; i++ ) {
		w = imgs[i].getAttribute( 'width' );
		imgs[i].removeAttribute( 'width' );
		imgs[i].removeAttribute( 'height' );
	}
	});
	</script>

	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/excanvas.js"></script>

	<![endif]-->

	<!--[if lte IE 9]>
	<script type="text/javascript">
	jQuery(document).ready(function() {

	// Combine inline styles for body tag
	jQuery('body').each( function() {
		var combined_styles = '<style type="text/css">';

		jQuery( this ).find( 'style' ).each( function() {
			combined_styles += jQuery(this).html();
			jQuery(this).remove();
		});

		combined_styles += '</style>';

		jQuery( this ).prepend( combined_styles );
	});
	});
	</script>

	<![endif]-->

	<script type="text/javascript">
		var doc = document.documentElement;
		doc.setAttribute('data-useragent', navigator.userAgent);
	</script>

	<?php echo Avada()->settings->get( 'google_analytics' ); ?>

	<?php echo Avada()->settings->get( 'space_head' ); ?>

</head>
<?php
$wrapper_class = '';


if ( is_page_template( 'blank.php' ) ) {
	$wrapper_class  = 'wrapper_blank';
}

if ( 'modern' == Avada()->settings->get( 'mobile_menu_design' ) ) {
	$mobile_logo_pos = strtolower( Avada()->settings->get( 'logo_alignment' ) );
	if ( 'center' == strtolower( Avada()->settings->get( 'logo_alignment' ) ) ) {
		$mobile_logo_pos = 'left';
	}
}

?>
<body <?php body_class(); ?> data-spy="scroll">
	<?php $boxed_side_header_right = false; ?>
	<?php if ( ( ( 'Boxed' == Avada()->settings->get( 'layout' ) && ( 'default' == get_post_meta( $c_pageID, 'pyre_page_bg_layout', true ) || '' == get_post_meta( $c_pageID, 'pyre_page_bg_layout', true ) ) ) || 'boxed' == get_post_meta( $c_pageID, 'pyre_page_bg_layout', true ) ) && 'Top' != Avada()->settings->get( 'header_position' ) ) : ?>
		<?php if ( Avada()->settings->get( 'slidingbar_widgets' ) && ! is_page_template( 'blank.php' ) && ( 'Right' == Avada()->settings->get( 'header_position' ) || 'Left' == Avada()->settings->get( 'header_position' ) ) ) : ?>
			<?php get_template_part( 'slidingbar' ); ?>
			<?php $boxed_side_header_right = true; ?>
		<?php endif; ?>
		<div id="boxed-wrapper">
	<?php endif; ?>
	<div id="wrapper" class="<?php echo $wrapper_class; ?>">
		<div id="home" style="position:relative;top:1px;"></div>
		<?php if ( Avada()->settings->get( 'slidingbar_widgets' ) && ! is_page_template( 'blank.php' ) && ! $boxed_side_header_right ) : ?>
			<?php get_template_part( 'slidingbar' ); ?>
		<?php endif; ?>
		<?php if ( false !== strpos( Avada()->settings->get( 'footer_special_effects' ), 'footer_sticky' ) ) : ?>
			<div class="above-footer-wrapper">
		<?php endif; ?>

		<?php avada_header_template( 'Below' ); ?>
		<?php if ( 'Left' == Avada()->settings->get( 'header_position' ) || 'Right' == Avada()->settings->get( 'header_position' ) ) : ?>
			<?php avada_side_header(); ?>
		<?php endif; ?>

		<div id="sliders-container">
			<?php
			if ( is_search() ) {
				$slider_page_id = '';
			} else {
				// Layer Slider
				$slider_page_id = '';
				if ( ! is_home() && ! is_front_page() && ! is_archive() && isset( $object_id ) ) {
					$slider_page_id = $object_id;
				}
				if ( ! is_home() && is_front_page() && isset( $object_id ) ) {
					$slider_page_id = $object_id;
				}
				if ( is_home() && ! is_front_page() ) {
					$slider_page_id = get_option( 'page_for_posts' );
				}
				if ( class_exists( 'WooCommerce' ) && is_shop() ) {
					$slider_page_id = get_option( 'woocommerce_shop_page_id' );
				}
				avada_slider( $slider_page_id );
			} ?>
		</div>
		<?php if ( get_post_meta( $slider_page_id, 'pyre_fallback', true ) ) : ?>
			<div id="fallback-slide">
				<img src="<?php echo get_post_meta( $slider_page_id, 'pyre_fallback', true ); ?>" alt="" />
			</div>
		<?php endif; ?>
		<?php avada_header_template( 'Above' ); ?>

		<?php if ( has_action( 'avada_override_current_page_title_bar' ) ) : ?>
			<?php do_action( 'avada_override_current_page_title_bar', $c_pageID ); ?>
		<?php else : ?>
			<?php avada_current_page_title_bar( $c_pageID ); ?>
		<?php endif; ?>

		<?php if ( is_page_template( 'contact.php' ) && Avada()->settings->get( 'recaptcha_public' ) && Avada()->settings->get( 'recaptcha_private' ) ) : ?>
			<script type="text/javascript">var RecaptchaOptions = { theme : '<?php echo Avada()->settings->get( 'recaptcha_color_scheme' ); ?>' };</script>
		<?php endif; ?>

		<?php if ( is_page_template( 'contact.php' ) && Avada()->settings->get( 'gmap_address' ) && ! Avada()->settings->get( 'status_gmap' ) ) : ?>
			<?php
			$map_popup             = ( ! Avada()->settings->get( 'map_popup' ) )        ? 'yes' : 'no';
			$map_scrollwheel       = ( ! Avada()->settings->get( 'map_scrollwheel' ) )  ? 'yes' : 'no';
			$map_scale             = ( ! Avada()->settings->get( 'map_scale' ) )        ? 'yes' : 'no';
			$map_zoomcontrol       = ( ! Avada()->settings->get( 'map_zoomcontrol' ) )  ? 'yes' : 'no';
			$address_pin           = ( ! Avada()->settings->get( 'map_pin' ) )          ? 'yes' : 'no';
			$address_pin_animation = ( Avada()->settings->get( 'gmap_pin_animation' ) ) ? 'yes' : 'no';
			?>
			<div id="fusion-gmap-container">
				<?php echo do_shortcode( '[avada_map address="' . Avada()->settings->get( 'gmap_address' ) . '" type="' . Avada()->settings->get( 'gmap_type' ) . '" address_pin="' . $address_pin . '" animation="' .  $address_pin_animation . '" map_style="' . Avada()->settings->get( 'map_styling' ) . '" overlay_color="' . Avada()->settings->get( 'map_overlay_color' ) . '" infobox="' . Avada()->settings->get( 'map_infobox_styling' ) . '" infobox_background_color="' . Avada()->settings->get( 'map_infobox_bg_color' ) . '" infobox_text_color="' . Avada()->settings->get( 'map_infobox_text_color' ) . '" infobox_content="' . htmlentities( Avada()->settings->get( 'map_infobox_content' ) ) . '" icon="' . Avada()->settings->get( 'map_custom_marker_icon' ) . '" width="' . Avada()->settings->get( 'gmap_width' ) . '" height="' . Avada()->settings->get( 'gmap_height' ) . '" zoom="' . Avada()->settings->get( 'map_zoom_level' ) . '" scrollwheel="' . $map_scrollwheel . '" scale="' . $map_scale . '" zoom_pancontrol="' . $map_zoomcontrol . '" popup="' . $map_popup . '"][/avada_map]' ); ?>
			</div>
		<?php endif; ?>

		<?php if ( is_page_template( 'contact-2.php' ) && Avada()->settings->get( 'gmap_address' ) && ! Avada()->settings->get( 'status_gmap' ) ) : ?>
			<?php
			$map_popup             = ( Avada()->settings->get( 'map_popup' ) )          ? 'yes' : 'no';
			$map_scrollwheel       = ( ! Avada()->settings->get( 'map_scrollwheel' ) )  ? 'yes' : 'no';
			$map_scale             = ( ! Avada()->settings->get( 'map_scale' ) )        ? 'yes' : 'no';
			$map_zoomcontrol       = ( ! Avada()->settings->get( 'map_zoomcontrol' ) )  ? 'yes' : 'no';
			$address_pin_animation = ( Avada()->settings->get( 'gmap_pin_animation' ) ) ? 'yes' : 'no';
			?>
			<div id="fusion-gmap-container">
				<?php echo do_shortcode( '[avada_map address="' . Avada()->settings->get( 'gmap_address' ) . '" type="' . Avada()->settings->get( 'gmap_type' ) . '" map_style="' . Avada()->settings->get( 'map_styling' ) . '" animation="' .  $address_pin_animation . '" overlay_color="' . Avada()->settings->get( 'map_overlay_color' ) . '" infobox="' . Avada()->settings->get( 'map_infobox_styling' ) . '" infobox_background_color="' . Avada()->settings->get( 'map_infobox_bg_color' ) . '" infobox_text_color="' . Avada()->settings->get( 'map_infobox_text_color' ) . '" infobox_content="' . Avada()->settings->get( 'map_infobox_content' ) . '" icon="' . Avada()->settings->get( 'map_custom_marker_icon' ) . '" width="' . Avada()->settings->get( 'gmap_width' ) . '" height="' . Avada()->settings->get( 'gmap_height' ) . '" zoom="' . Avada()->settings->get( 'map_zoom_level' ) . '" scrollwheel="' . $map_scrollwheel . '" scale="' . $map_scale . '" zoom_pancontrol="' . $map_zoomcontrol . '" popup="' . $map_popup . '"][/avada_map]' ); ?>
			</div>
		<?php endif; ?>
		<?php
		$main_css      = '';
		$row_css       = '';
		$main_class    = '';
		$page_template = '';

		if ( function_exists( 'is_woocommerce' ) && is_woocommerce() ) {
			$custom_fields = get_post_custom_values( '_wp_page_template', $c_pageID );
			$page_template = ( is_array( $custom_fields ) && ! empty( $custom_fields ) ) ? $custom_fields[0] : '';
		}

		if ( get_post_type( $c_pageID ) == 'tribe_events' && tribe_get_option( 'tribeEventsTemplate', 'default' ) == '100-width.php' ) {
			$page_template = '100-width.php';
		}

		if (
			is_page_template( '100-width.php' ) ||
			is_page_template( 'blank.php' ) ||
			'100-width.php' == $page_template ||
			( ( '1' == fusion_get_option( 'portfolio_width_100', 'portfolio_width_100', $c_pageID ) || 'yes' == fusion_get_option( 'portfolio_width_100', 'portfolio_width_100', $c_pageID ) ) && ( 'avada_portfolio' == get_post_type( $c_pageID ) ) ) ||
			( ( '1' == fusion_get_option( 'blog_width_100', 'portfolio_width_100', $c_pageID ) || 'yes' == fusion_get_option( 'blog_width_100', 'portfolio_width_100', $c_pageID ) ) && ( 'post' == get_post_type( $c_pageID ) ) ) ||
			( 'yes' == fusion_get_page_option( 'portfolio_width_100', $c_pageID ) && ( 'post' != get_post_type( $c_pageID ) && 'avada_portfolio' != get_post_type( $c_pageID ) ) ) ||
			( avada_is_portfolio_template() && 'yes' == get_post_meta( $c_pageID, 'pyre_portfolio_width_100', true ) )
		) {
			$main_css = 'padding-left:0px;padding-right:0px;';
			if ( Avada()->settings->get( 'hundredp_padding' ) && ! get_post_meta( $c_pageID, 'pyre_hundredp_padding', true ) ) {
				$main_css = 'padding-left:' . Avada()->settings->get( 'hundredp_padding' ) . ';padding-right:' . Avada()->settings->get( 'hundredp_padding' );
			}
			if ( get_post_meta( $c_pageID, 'pyre_hundredp_padding', true ) ) {
				$main_css = 'padding-left:' . get_post_meta( $c_pageID, 'pyre_hundredp_padding', true ) . ';padding-right:' . get_post_meta( $c_pageID, 'pyre_hundredp_padding', true );
			}
			$row_css    = 'max-width:100%;';
			$main_class = 'width-100';
		}
		do_action( 'avada_before_main' );
		?>
		<?php if ( is_front_page() ) { ?>
		<div class="formholder">
			<h1>GIO Klassiekerverzekering</h1>
			<form action="/verzekeringen/klassieke-autoverzekering/premie-berekenen/" method="POST">
				<input type="text" name="kenteken" size="10" value=""/>
				<input type="submit" value="Bereken uw premie"/>
			</form>
			<form action="/maatwerk-offerte-opvragen/" method="POST" class="maatwerk">
				<input type="text" name="kenteken" size="10" value=""/>
				<input type="submit" value="Maatwerkofferte"/>
			</form>
			<form action="https://www.taxeren-online.nl/" target="_blank" class="maatwerk">
				<input type="text" size="10" value=""/>
				<input type="submit" value="Taxeren Online"/>
			</form>
			<form action="/service-contact/voertuig-wijziging-doorgeven/" class="maatwerk">
				<input type="text" size="10" value=""/>
				<input type="submit" value="Voertuig wijziging doorgeven"/>
			</form>
		</div>
		<?php }	?>

		<script type="text/javascript">
			$(document).ready(function() {
				
				$('input[name="kenteken"]').keyup(function(){
    				$(this).val($(this).val().toUpperCase());
				});

				if(window.location.href.indexOf("/verzekeringen/moderne-autoverzekering/premie-berekenen/") > -1) {

					var kenteken = "<?php echo $_SESSION['kenteken']; ?>"; 
					console.log(kenteken);
					var merk = "<?php echo $_SESSION['merk']; ?>";
					console.log(merk);
					var naam = "<?php echo $_SESSION['handelsbenaming']; ?>";
					console.log(naam);
					var bouwjaar = "<?php echo $_SESSION['bouwjaar']; ?>";
					console.log(bouwjaar);
					var gewicht = "<?php echo $_SESSION['lediggewicht']; ?>";
					console.log(gewicht);
					var nieuwaarde ="<?php echo $_SESSION['nieuwwaarde']; ?>";
					console.log(nieuwaarde);
					var motorinhoud = "<?php echo $_SESSION['motorinhoud']; ?>";
					console.log(motorinhoud);
					<?php

						unset($_SESSION['kenteken']);
						unset($_SESSION['merk']);
						unset($_SESSION['handelsbenaming']);
						unset($_SESSION['bouwjaar']);
						unset($_SESSION['lediggewicht']);
						unset($_SESSION['nieuwwaarde']);
						unset($_SESSION['motorinhoud']);
						
					?>
					// alert("Kenteken = " + kenteken + " Merk = " + merk + " Type = " + naam );
					$('.kenteken').find('input').val(kenteken);
					$('.merk').find('input').val(merk);
					$('.type').find('input').val(naam);
					$('.bouwjaar').find('input').val(bouwjaar);
					$('.lediggewicht').find('input').val(gewicht);
					$('.Nieuwwaarde').find('input').val(nieuwaarde);
					$('.motorinhoud').find('input').val(motorinhoud);

					var cookies = document.cookie.split(";");

					for (var i = 0; i < cookies.length; i++) {
						var cookie = cookies[i];
						var eqPos = cookie.indexOf("=");
						var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
						document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
					}
					
				}

				$('input[name="kenteken"]').keyup(function(){
					if ($(this).val().length == 6) {

						var postkent = $(this).val().toUpperCase();
						var currLoc = window.location.href;
						var dataString = {
							kenteken: postkent
						}

						$.ajax({
							url: "https://opendata.rdw.nl/resource/m9d7-ebf2.json",
							method: "GET",
							dataType: "json",
							data: {
								"kenteken": postkent,
								"$$app_token": 'aHEWbjMHCJfM2WpVla41EQtUy'
							},
							success: function( data, status, jqxhr ){
								$('.merk input').val(data[0].merk);
								//console.log(data[0].merk);
								$('.type input').val(data[0].handelsbenaming);
								//console.log(data[0].handelsbenaming);
								$('.bouwjaar input').val(data[0].datum_eerste_afgifte_nederland);
								//console.log(data[0].datum_eerste_afgifte_nederland);
								$('.lediggewicht input').val(data[0].massa_ledig_voertuig);
								//console.log(data[0].massa_ledig_voertuig);
								$('.Nieuwwaarde input').val(data[0].catalogusprijs);
								//console.log(data[0].catalogusprijs);
								$('.motorinhoud input').val(data[0].cilinderinhoud);
								//console.log(data[0].cilinderinhoud);
							},
							error: function( jqxhr, status, error ){
								console.log( "Neem contact op met S&S Online Marketing." );
								console.log( "Kan geen verbinding maken met de Socrata API." );
							}
						});
					}
				});
				
				$('input[name="kenteken"]').change(function(){
					if ($(this).val().length == 6) {

						var postkent = $(this).val().toUpperCase();
						var currLoc = window.location.href;
						var dataString = {
							kenteken: postkent
						}

						$.ajax({
							url: "https://opendata.rdw.nl/resource/m9d7-ebf2.json",
							method: "GET",
							dataType: "json",
							data: {
								"kenteken": postkent,
								"$$app_token": 'aHEWbjMHCJfM2WpVla41EQtUy'
							},
							success: function( data, status, jqxhr ){
								$('.merk input').val(data[0].merk);
								//console.log(data[0].merk);
								$('.type input').val(data[0].handelsbenaming);
								//console.log(data[0].handelsbenaming);
								$('.bouwjaar input').val(data[0].datum_eerste_afgifte_nederland);
								//console.log(data[0].datum_eerste_afgifte_nederland);
								$('.lediggewicht input').val(data[0].massa_ledig_voertuig);
								//console.log(data[0].massa_ledig_voertuig);
								$('.Nieuwwaarde input').val(data[0].catalogusprijs);
								//console.log(data[0].catalogusprijs);
								$('.motorinhoud input').val(data[0].cilinderinhoud);
								//console.log(data[0].cilinderinhoud);
							},
							error: function( jqxhr, status, error ){
								console.log( "Neem contact op met S&S Online Marketing." );
								console.log( "Kan geen verbinding maken met de Socrata API." );
							}
						});
					}
				});

				$('#_1').change(function() {
					if ($("#_1").is(':checked')){
						console.log("Show");
						$("#merk").show();
						$("#type").show();
						$("#bouwjaar").show();
						$("#gewicht").show();
						$("#waarde").show();
						$("#motorinhoud").show();
					} else {
						console.log("hide");
						$("#merk").hide();
						$("#type").hide();
						$("#bouwjaar").hide();
						$("#gewicht").hide();
						$("#waarde").hide();
						$("#motorinhoud").hide();
					}
				});
			});
		</script>

		<div id="main" class="clearfix <?php echo $main_class; ?>" style="<?php echo $main_css; ?>">
			<div class="fusion-row" style="<?php echo $row_css; ?>">